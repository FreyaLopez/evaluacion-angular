# Evaluación Angular

En este documento, te proporcionaremos una breve descripción del proyecto y sus principales características.

## Resumen

Este proyecto se desarrollará utilizando Angular, un popular framework de desarrollo web. En combinación con Angular, utilizaremos PHP para implementar un pequeño sistema que cuenta con un sistema de inicio de sesión. Es importante destacar que este sistema de inicio de sesión utilizará datos fijos y no se realizará validación contra una API o base de datos en esta etapa.

## Descripción del Proyecto

El objetivo principal de este proyecto es crear un sistema web que permita a los usuarios autenticados acceder a funcionalidades "premium", mientras que los usuarios no autenticados podrán acceder a las funcionalidades principales de manera limitada como invitados.

### Funcionalidades del Sistema

El sistema deberá proporcionar las siguientes funcionalidades:

1. **Listado de Autores:** Los usuarios autenticados podrán listar a los autores de poesía mediante una llamada a la API: [https://poetrydb.org/author](https://poetrydb.org/author).

2. **Obras por Autor:** Los usuarios autenticados podrán seleccionar un autor y ver las obras asociadas a ese autor mediante la API: [https://poetrydb.org/author/[AUTOR]/title](https://poetrydb.org/author/[AUTOR]/title).

### Autor del Proyecto

Este proyecto está siendo desarrollado por Freya Milena Lopez Lopez.

## Modo Invitado

Los usuarios no autenticados tendrán acceso al "Modo Invitado", donde podrán explorar las funcionalidades principales del sistema. Sin embargo, las funcionalidades "premium" estarán desactivadas y solo podrán acceder a un conjunto limitado de características.

---


